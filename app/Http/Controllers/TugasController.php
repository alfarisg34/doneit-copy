<?php

namespace App\Http\Controllers;

use App\Http\Controllers\LogbookController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Tugas;
use App\Models\User;
use App\Models\Forum;
use App\Models\Reply;

class TugasController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => []]);
    }
    public function timeZone($location){
        return date_default_timezone_set($location);
    }
    public function index(){
        $tugas = DB::table('tugas')->get();
        // $unDone = DB::table('tugas')
        //             ->where('progress','=',0)
        //             ->get();    
        // $done = DB::table('tugas')
        //             ->where('progress','!=',0)
        //             ->get();
        $users=DB::table('users')->get();
        $this->timeZone('Asia/Jakarta');
        $date = date("Y-m-d");
        return view('admin.penugasan', compact('users','date','tugas'));
    }
    public function tugasAction(Request $request)
    {
        $this->timeZone('Asia/Jakarta');
        $date = date("Y-m-d");
        try{
            DB::beginTransaction();
            
            $tugas = new Tugas;
            $tugas->title = $request->title;
            $tugas->desc = $request->desc;
            $tugas->dueTo = $request->dueTo;
            $tugas->user_id = $request->user_id;
            $tugas->progress = 0;
            $tugas->save();

            $forum = new Forum;
            $forum -> user_id = $request->user_id;
            $forum -> title = $request->title;
            $forum -> desc = $request->desc;
            $forum -> date = $date;
            $forum -> save();
            
            DB::commit();
            
            $id = $request -> user_id;
            $status = $request -> status;
            $affected = DB::table('users')
              ->where('user_id', $id)
              ->update(['status' => $status]);

            $affected = DB::table('absens')
              ->where('user_id', $id)
              ->update(['status' => $status]);

        } catch(Exception $e){
            DB::rollBack();
			$output = $e->getMessage();
			return redirect('/penugasan')->withErrors(['msg', $output]);
        }
        return redirect('/penugasan');
    }
    public function adminCheckAction(Request $request, $id)
    {
        try{
            $progress = $request->checked;
            $affected = DB::table('tugas')
                  ->where('id', $id)
                  ->update(['progress' => $progress]);
    
            }catch(Exception $e){
                    DB::rollBack();
                    $output = $e->getMessage();
                    return redirect('/penugasan')->withErrors(['msg', $output]);
                }
            return redirect('/penugasan');
    }
    public function modalTaskNow($id){
        $dataTugas = Tugas::find($id);
        return view('admin.modals.modalPenugasanNow',['dataTugas' => $dataTugas]);
    }
    public function modalTaskPrev($id){
        $dataTugas = Tugas::find($id);
        return view('admin.modals.modalPenugasanPrev',['dataTugas' => $dataTugas]);
    }
}
