<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;

use App\Models\Tugas;
use App\Models\User;
use App\Models\Forum;
use App\Models\Reply;

class LogbookController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => []]);
    }
    public function timeZone($location){
        return date_default_timezone_set($location);
    }
    public function logbook()
    {
        $this->timeZone('Asia/Jakarta');
        $date = date("Y-m-d");
        $user_id = Auth::user()->user_id;
        $unDone = DB::table('tugas')
                    ->where('user_id',$user_id)
                    ->where('progress','=',0)
                    ->get();    
        $done = DB::table('tugas')
                    ->where('user_id',$user_id)
                    ->where('progress','!=',0)
                    ->get();
        $desc = DB::table('tugas')->get();
        return view('user.logbookUser',compact('done','unDone','date','desc'));
    }
    public function userCheckAction(Request $request,$id)
    {
        try{
        $progress = $request->checked;
        $affected = DB::table('tugas')
              ->where('id', $id)
              ->update(['progress' => $progress]);

        }catch(Exception $e){
                DB::rollBack();
                $output = $e->getMessage();
                return redirect('/logbook')->withErrors(['msg', $output]);
            }
        return redirect('/logbook');
    }
    
    public function modalTask($id){
        $dataTugas = Tugas::find($id);
        return view('user.ModalLogbook.logbookModalPrev',['dataTugas' => $dataTugas]);
    }
}