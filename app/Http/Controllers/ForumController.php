<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;

use App\Models\Tugas;
use App\Models\User;
use App\Models\Forum;
use App\Models\Reply;

class ForumController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => []]);
    }
    public function forum()
    {
        $forums = DB::table('forums')->get();
        return view('user.forum',['forums' => $forums]);
    }
    public function detailforum($id)
    {   
        $count = DB::table('replies')->count();
        if($count!=0){
            $reply = DB::table('replies')
                    ->where('id_forum',$id)
                    ->get();    
        }else{
            $reply = DB::table('replies')
                    ->get();    
        }
        $forums = DB::table('forums')
                    ->where('id',$id)
                    ->get();    
        return view('user.detailuser',compact('forums','reply'));
    }

    public function forumAdmin()
    {   
        $forums = DB::table('forums')->get();
        return view('admin.forumadmin',compact('forums'));
    }
    public function detailForumAdmin($id)
    {
        $count = DB::table('replies')->count();
        if($count!=0){
            $reply = DB::table('replies')
                    ->where('id_forum',$id)
                    ->get();    
        }else{
            $reply = DB::table('replies')
                    ->get();    
        }
        $forums = DB::table('forums')
                    ->where('id',$id)
                    ->get();    
        return view('admin.detailforum',compact('forums','reply'));
    }
    public function replyAction(Request $request,$id)
    {
        try{
            DB::beginTransaction();
            
            $reply = new Reply;
            $reply -> user_id = Auth::user()->user_id;
            $reply -> name = Auth::user()->name;
            $reply -> desc = $request->desc;
            $reply -> id_forum = $id;
            $reply -> save();
            
            DB::commit();
        } catch(Exception $e){
            DB::rollBack();
			$output = $e->getMessage();
			return redirect('/detailforum'.'/'.$id)->withErrors(['msg', $output]);
        }
        return redirect('/detailforum'.'/'.$id);
    }
    public function replyActionAdmin(Request $request,$id)
    {
        try{
            DB::beginTransaction();
            
            $reply = new Reply;
            $reply -> user_id = Auth::user()->user_id;
            $reply -> name = Auth::user()->name;
            $reply -> desc = $request->desc;
            $reply -> id_forum = $id;
            $reply -> save();
            
            DB::commit();
        } catch(Exception $e){
            DB::rollBack();
			$output = $e->getMessage();
			return redirect('/detailforumadmin'.'/'.$id)->withErrors(['msg', $output]);
        }
        return redirect('/detailforumadmin'.'/'.$id);
    }
}