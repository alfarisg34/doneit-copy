<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\ProfileController;

use App\Models\User;
use App\Models\UserType;

class AuthController extends Controller
{

    // public function __construct() {
    //     $this->middleware('auth', ['only' => ['logout']]);
    // }

    public function index(Request $request)
    {
        if (Auth::check()){
            if(Auth::user()->isAdmin())
            return redirect('/dashboard');
            else
            return redirect('/absen');
            
        }else{
            return view('landing');
        }
    }
    //register
    public function register(Request $request)
    {
        return view('auth.register_page');
        Request()->validate([
            'username' => 'required|unique:users,username',
            'name' => 'required',
            'nip' => 'required|unique:users,user_id',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            // 'password' => 'required_with:passwordconf|same:passwordconf',
            // 'passwordconf' => 'required'
        ]);
    }
    public function registerAction(Request $request)
    {
        try{
            DB::beginTransaction();
            
            $user = new User;
            $user->username = $request->username;
            $user->name = $request->name;
            $user->role_id = 0;
            $user->pic = null;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->user_id = $request->user_id;
            $user->salary = null;
            $user->pic = 'default.png';
            // $user->date_create = null;
            // $user->page_slug = $this->nameToSlug($request->page_name);
            $user->save();

            DB::commit();
        } catch(Exception $e){
            DB::rollBack();
			$output = $e->getMessage();
			return redirect('/register')->withErrors(['msg', $output]);
        }
        $credentials = $request->only('email', 'password');
        Auth::attempt($credentials);
        return redirect('/');
    }

    //login
    public function login()
    {
        return view('auth.login_page');
    }

    public function loginAction(Request $request)
    {
        Request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            return back()->withErrors([
                'email' => 'Email or password is incorrect',
            ]);
            // return redirect('/login');
        }
    }

    //logout
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
    
}