<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;

use App\Models\User;
use App\Models\UserType;
use App\Models\Absen;

class AbsenController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => []]);
    }
    public function timeZone($location){
        return date_default_timezone_set($location);
    }
    public function absen(Request $request)
    {
        $this->timeZone('Asia/Jakarta');
        $user_id = Auth::user()->user_id;
        $date = date("Y-m-d");
        $cek_absen = Absen::where(['user_id' => $user_id, 'date' => $date])
                            ->get()
                            ->first();
        if (is_null($cek_absen)) {
            $info = array(
                "status1" => "Not Yet!",
                "status2" => "Not Yet!",
                "btnIn" => "",
                "btnOut" => "disabled");
        } elseif ($cek_absen->time_out == NULL) {
            $info = array(
                "status1" => "Done",
                "status2" => "Not Yet!",
                "btnIn" => "disabled",
                "btnOut" => "");
        } else {
            $info = array(
                "status1" => "Done",
                "status2" => "Done",
                "btnIn" => "disabled",
                "btnOut" => "disabled");
        }
        $name = User::where('user_id', $user_id);

        $data_absen = Absen::where('user_id', $user_id)
                        ->orderBy('date', 'desc')
                        ->paginate(2);
        return view('user.absensi', compact('data_absen', 'info','name','date'));
        // return view('auth.register_page');
    }
    
    public function absenAction(Request $request)
    {
        try{
            $this->timeZone('Asia/Jakarta');
            $user_id = Auth::user()->user_id;
            $date = date("Y-m-d"); // 2017-02-01
            $time = date("H:i:s"); // 12:31:20
            $status = Auth::user()->status;

            // $note = $request->note;
            var_dump($user_id);
            var_dump($date);
            var_dump($time);
            $absen = new Absen;
            // absen masuk

            if (isset($request->btnIn)){
                //cek double data
                $cek_double = $absen->where(['date'=> $date, 'user_id' => $user_id])->count();
                
                if ($cek_double >0 ){
                    return redirect()->back();
                }

                $absen->create([
                    'user_id'   => $user_id,
                    'date'      => $date,
                    'time_in'   => $time,
                    'status'    => $status,
                    'progress'    => '100'
                    // 'note'      => $note
                ]);

                return redirect()->back();

            } //absen keluar 
            elseif (isset($request->btnOut)){
                $absen->where(['date' => $date, 'user_id' => $user_id])
                    ->update([
                        'time_out' => $time,
                        // 'note'     => $note
                        ]);
                return redirect()->back();       
            }
            return $request->all();

        } catch(Exception $e){
            DB::rollBack();
			$output = $e->getMessage();
			return redirect('/absensi')->withErrors(['msg', $output]);
        }
        // $credentials = $request->only('email', 'password');
        // Auth::attempt($credentials);
        return redirect('/homepage');
    }

    public function absenAdmin(){
        $tanggal = DB::table('absens')
                    ->select('date')
                    ->groupBy('date')
                    ->orderBy('date','desc')
                    ->get();

        $usercount = DB::table('users')->where('role_id',0)->count();
        $hasil_absen = array();
        $present = array();
        $namaUser = array();

        foreach($tanggal as $data){
            $array_detail = DB::table('absens')
                            ->join('users', 'users.user_id', '=', 'absens.user_id')
                            ->where('date', $data->date)
                            ->orderBy('time_in', 'desc')
                            ->get();
            
            array_push($hasil_absen, $array_detail);
        }
        $i=0;
        $absent=array();
        foreach($hasil_absen as $data_hasil){
            $count=0;
            foreach($data_hasil as $data){
                $count++;
            }
            $present[$i] = $count;
            $absent[$i] = $usercount - $present[$i]; 
            $i++;
        }

        return view('admin.rekapabsen',compact('tanggal','hasil_absen','present','absent'));
    }
}
