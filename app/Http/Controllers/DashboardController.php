<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Http\Controllers\AbsenController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TugasController;

use App\Models\User;
use App\Models\UserType;
use App\Models\Absen;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => []]);
    }
    public function timeZone($location){
        return date_default_timezone_set($location);
    }

    public function index()
    {
        // $user = Auth::user()->isCreator();
        // $user = User::isCreator();
        // dd($user);
        // $user = User::get();
        $this->timeZone('Asia/Jakarta');
        $date = date("Y-m-d");
        $tanggal = DB::table('tugas')
                    ->select('dueTo')
                    ->groupBy('dueTo')
                    ->orderBy('dueTo','desc')
                    ->paginate(7);
        $vTugas = array();
        
        $i=0;
        foreach($tanggal as $data){
            $array_detail = DB::table('tugas')
                            ->select('progress')
                            ->where('dueTo', $data->dueTo)
                            ->orderBy('dueTo', 'asc')
                            ->get();
            array_push($vTugas, $array_detail);
        }

        $tanggalJson=array();
        foreach($tanggal as $dataTanggal){
            foreach($dataTanggal as $dataFix){
                $tanggalJson[$i] = $dataFix;
                $i++;
            }
        }


        $value=array();
        $avg=array();
        $sum=0;
        $i=0;
        foreach ($vTugas as $vTugas1) {
            $j=0;
            foreach($vTugas1 as $data){
                $value[$j]=$data->progress;
                $sum=$sum+$value[$j];
                $j++;
            }
            $avg[$i]=$sum/($j);
            $i++;
            $sum=0;
        }

        $data_absen = DB::table('absens')
                    ->join('users', 'users.user_id', '=', 'absens.user_id')
                    ->where('date',$date)
                    ->get();

        $data_forum = DB::table('forums')->get();

        $replycount=array();
        $i=0;
        foreach($data_forum as $forum){
            $replycount[$i] = DB::table('replies')
                    ->where('id_forum',$forum->id)
                    ->count();
            $i++;
        }

        $logActivity = DB::table('users')
            ->join('tugas', 'users.user_id', '=', 'tugas.user_id')
            ->select('users.name', 'tugas.title', 'tugas.progress')
            ->where('tugas.dueTo',$date)
            ->get();

        return view('admin.dashboard',compact('data_absen','date','data_forum','replycount','logActivity'))->with('tanggalJson',json_encode($tanggalJson,JSON_NUMERIC_CHECK))->with('avg',json_encode($avg,JSON_NUMERIC_CHECK));
    }

    // public function chart()
    // {
    //     $tanggal = DB::table('absens')
    //                 ->select('date')
    //                 ->groupBy('date')
    //                 ->orderBy('date','desc')
    //                 ->get();
    //     $date[];
    //     foreach ($month as $key => $value) {
    //         $user[] = User::where(\DB::raw("DATE_FORMAT(created_at, '%Y')"),$value)->count();
    //     }
    //     $user = [];
    //     foreach ($month as $key => $value) {
    //         $user[] = User::where(\DB::raw("DATE_FORMAT(created_at, '%Y')"),$value)->count();
    //     }

    // 	return view('chartjs')->with('month',json_encode($month,JSON_NUMERIC_CHECK))->with('user',json_encode($user,JSON_NUMERIC_CHECK));
    // }
}
