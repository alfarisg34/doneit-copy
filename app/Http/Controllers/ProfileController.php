<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Requests\UpdateProfileRequest;

use App\Models\User;
use App\Models\UserType;

/**
 * Update user's profile
 *
 * @param  UpdateProfileRequest $request
 * @return \Illuminate\Contracts\Support\Renderable
 */

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => []]);
    }
    public function edit(Request $request)
    {
        if(Auth::user()->isAdmin())
        return view('admin.profileadmin', [
            'user' => $request->user()
        ]);
            else
            return view('user.profile', [
                'user' => $request->user()
            ]);
        // return view('user.profile', [
        //     'user' => $request->user()
        // ]);
    }
    public function update(Request $request)
    {
        Request()->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        // $request->user()->update(
        //     $request->all()
        // );
        $user_id = Auth::user()->user_id;
        DB::table('users')
            ->where('user_id',$user_id)
        //     ->updateOrInsert([
        //         'email' => $request->email, 
        //         'name' => $request->name,
        //         'address' => $request->address,
        //         'phone' => $request->phone,
        //         'company' => $request->company,
        // ]);
        ->update([
            'email' => $request->email, 
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'company' => $request->company,
        ]);

        if(Auth::user()->isAdmin())
            return redirect()->route('admin.profileadmin');
            else
            return redirect()->route('user.profile');
        // return redirect()->route('user.profile');
    }
}
