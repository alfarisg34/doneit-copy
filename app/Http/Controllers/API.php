<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class API extends Controller
{
    function getDataForums()
    {
        $data = DB::table('forums')->get();
        return $data;
    }
    function getDataReplies()
    {
        $data = DB::table('replies')->get();
        return $data;
    }
    function getDataAbsens()
    {
        $data = DB::table('absens')->get();
        return $data;
    }
    function getDataUsers()
    {
        $data = DB::table('users')->get();
        return $data;
    }
    
}
