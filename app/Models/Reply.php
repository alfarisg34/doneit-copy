<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'name', 
        'desc',
        'id_forum', 
        'status',
        ];
        public function forum()
        {
    	    return $this->belongsTo(Forum::class, 'id');
        }
}
