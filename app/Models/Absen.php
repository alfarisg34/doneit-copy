<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'date', 
        'time_in', 
        'time_out',
        'status', 
        'progress',
        'feedback'];

    // public function isAbsenMasuk(){
    //     if($this->is_absen_masuk == 1){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
    // public function isAbsenPulang(){
    //     if($this->is_absen_pulang == 1){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
    public function isabsen()
    {
        return $this->hasMany(IsAbsen::class, 'id');
    }
}
