<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $primaryKey = 'user_id';
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'role_id',
        'pic',
        'email',
        'password',
        'user_id',
        'salary',
        // 'date_created',
    ];

    public function isAdmin(){
        if($this->role_id == 1){
            return true;
        }else{
            return false;
        }
    }

    public function userRole()
    {
        return $this->belongsTo(UserRole::class, 'role_id');
    }
    public function absen()
    {
        return $this->hasMany(Absen::class, 'user_id');
    }
    public function getProfilepictureFilenameAttribute()
    {
        if (! $this->attributes['pic']) {
            return '/images/default.png';
        }

        return $this->attributes['pic'];
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
