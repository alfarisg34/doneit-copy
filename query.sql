create database doneit;
use doneit;
create table account(
    username varchar(12) not null,
    name varchar(32) not null,
    role_id int(1) not null,
    pic varchar(200) not null,
    password varchar(128) not null,
    user_id varchar(12) not null, 
    email varchar(30) not null,
    date_created varchar(30) DEFAULT NULL,
    salary varchar(10) DEFAULT NULL,
    PRIMARY KEY (user_id)
);

-- create table absen(
--     kd_absen int not null auto_increment,
--     account_id varchar(12) not null,
--     feedback varchar(200) null,
--     is_absen_masuk int(1) DEFAULT '0',
--     is_absen_pulang int(1) DEFAULT '0',
--     time_stamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
--     FOREIGN KEY (account_id) REFERENCES account(account_id) ON Delete cascade,
--     PRIMARY KEY (kd_absen)
-- );
-- create table tugas(
--     kd_tugas int not null auto_increment, 
--     title varchar(200) not null,
--     account_id varchar(12) not null,
--     desc_tugas varchar(200) null,
--     date_created varchar(30) DEFAULT NULL,
--     is_done int(1) not null DEFAULT '0',
--     FOREIGN KEY (account_id) REFERENCES account(account_id) ON Delete cascade,
--     PRIMARY KEY(kd_tugas, account_id)
-- );
-- create table forum(
--     kd_forum int not null auto_increment, 
--     title varchar(200) not null,
--     account_id varchar(12) not null,
--     desc_tugas varchar(200) null,
--     date_created varchar(30) DEFAULT NULL,
--     FOREIGN KEY (account_id) REFERENCES account(account_id) ON Delete cascade,
--     PRIMARY KEY(kd_forum, account_id)
-- );
-- create table reply(
--     kd_reply int not null auto_increment, 
--     account_id varchar(12) not null,
--     desc_reply varchar(200) null,
--     date_created varchar(30) DEFAULT NULL,
--     kd_forum varchar(12) not null,
--     FOREIGN KEY (kd_forum) REFERENCES forum(kd_forum) ON Delete cascade,
--     PRIMARY KEY(kd_reply, account_id)
-- );


