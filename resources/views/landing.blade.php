@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/doneit.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/slick.css') }}">
@endsection

@section('custom_title')
    <title>DoneIt! | Aplikasi Manajemen Organisasi</title>
@endsection('custom_title')

@section('content')
  <section class="banner-home">
      <div class="container">
      <div class="separator-lg"></div>
        <div> 
          <p class="title text-center">Start Boost Your <br/> Productivity</p>
          <p class="text-center" style="font-size: 32px; color: #3A728D;">
            No more excuse, do it right now.
          </p>
        </div>
          <div class="separator-lg"></div>
          <div class="separator-lg"></div>
          <div class="separator-lg"></div>
          <div class="separator-lg"></div>
          <div class="separator-lg"></div>
          <div class="separator-lg"></div>
      </div>
  </section>

      <div class="container">
        <div class="row mt-5">
          <div class="col-xs-12 col-sm-6">
           <img src="{{ asset('/assets/img/logo-doneit-puzzle.png') }}" class="header-img" width="400" height="400" alt="logo-puzzle">
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="separator"></div>
            <div class="separator"></div>
            <div class="text-right">
              <div class="big-title">Done-It!</div>

              <div class="separator-md"></div>
              <p>
                Sebuah aplikasi task-management platform yang 
                mempermudahkah koordinasi terkait penugasan dan juga 
                absensi pada sebuah perusahaan atau organisasi yang 
                bersifat open source.
              </p>
            </div>
            <div class="separator-lg"></div>
          </div>
        </div>
      </div>

      <div class="container">
        <p class="subtitle text-center">Our Features</p>
        <div class="list center"></div>
        <div class="separator-md"></div>
        <p class="text-center" style="font-size: 32px; color: #000000;">
          Some Features to Help You More Productive 
        </p>
        <div class="separator"></div>
          <div id="slider-slick" class="card-slider">
            <div class="card">
              <div class="separator"></div>
              <img src="{{ asset('/assets/img/absen-me.png') }}" alt="Absen-me" class="center">
              <div class="separator"></div>
              <p class="text-center" style="font-size: 28px;">Absen-me</p>
            </div>
            <div class="card">
              <div class="separator"></div>
              <img src="{{ asset('/assets/img/myjob.png') }}" alt="My Job" class="center">
              <div class="separator"></div>
              <p class="text-center" style="font-size: 28px;">My Job</p>
            </div>
            <div class="card">
              <div class="separator"></div>
              <img src="{{ asset('/assets/img/discuss.png') }}" alt="Discuss" class="center">
              <div class="separator"></div>
              <p class="text-center" style="font-size: 28px;">Discuss</p>
            </div>
          </div>
        <div class="separator"></div>
        </div>
        </div>          
      </div>      

@endsection

@section('custom_js_bottom')
    <script type="text/javascript" src="{{ asset('/assets/js/jQuery-2.1.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/doneit.js') }}"></script>
@endsection