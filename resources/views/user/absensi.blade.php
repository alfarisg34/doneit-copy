@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/absensi.css') }}">
@endsection

@section('custom_title')
    <title>Absensi Pegawai</title>
@endsection('custom_title')

@section('content')
  <div class="container-fluid">
    <div class="row">

      <!-- Today Presence -->
      <div class="col-xs-12 col-sm-6">
        <div class="box">
          <h1 class="judul-section">Today's Presence </h1>
          <span class="tanggal mt-5">{{$date}}</span>
          <div class="row mt-5">
            <div class="col-sm-2 foto">
              <img class="user-foto" src="{{ asset('/assets/img/profile-foto.jpg') }}" alt="foto-profile">
            </div>
            <div class="col-sm-8 bio-desc">
              <p class="label">Name</p>
              <p class="content">{{ Auth::user()->name }}</p>
              <p class="label">NIP</p>
              <p class="content">{{ Auth::user()->user_id }}</p>
              <p class="label">Status</p>
              <p class="content">{{ Auth::user()->status }}</p>
            </div>
          </div>
          <p class="label">Check In :
            @if ( $info['status1'] === "Not Yet!" )
            <span class="ml-3" style="color: #DD6767">&nbsp&nbsp {{ $info['status1'] }}</span>
            @else
            <span class="ml-3" style="color: #68D16C">&nbsp&nbsp {{ $info['status1'] }}</span>
            @endif
          </p>
          <p class="label">Check Out :
            @if ( $info['status2'] === "Not Yet!" )
            <span class="ml-3" style="color: #DD6767">{{ $info['status2'] }}</span>
            @else
            <span class="ml-3" style="color: #68D16C">{{ $info['status2'] }}</span>
            @endif
          </p>
          <form action="/absen" method="post">
                {{csrf_field()}}
                @if ( $info['status1'] === "Not Yet!" )
                  <button type="submit" class="btn-absen" name="btnIn" id="btnIn" value="1" {{ $info['btnIn'] }}>
                    <i class="fas fa-clock"></i> <span class="mr-3"> Check In</span>
                  </button>
                @else
                  <button type="submit" class="btn-absen" name="btnOut" id="btnOut" value="2" {{ $info['btnOut'] }}>
                      <i class="fas fa-clock"></i> <span class="mr-3"> Check Out</span>
                  </button>
                @endif         
            </form>
          <div class="bg-blob">
          </div>
        </div>
      </div>

      <!-- History -->
      <div class="col-xs-12 col-sm-6">
        <div class="box box-right">
          <h1 class="judul-section">History </h1>

          @forelse($data_absen as $absen)

          <div class="card absen mt-5">
            <div class="card-body">
              <div class="card-title">
                <p class="alignleft"><b><i class="fas fa-clock"></i> <span class="mr-3">{{$absen->date}}</span></b></p>
                <p class="alignright">
                  In : {{$absen -> time_in}} WIB<br>
                  Out : {{$absen -> time_out}} WIB
                </p>
              </div>
              <div class="card-text">
                <p class="alignleft">&nbsp Present</p>
                <p class="alignright">{{$absen -> status}}</p>
                <div style="clear: both;"></div>
              </div>
            </div>
          </div>  
          @empty
          <div class="card absen">
            <div class="card-body">
              <div class="card-title">
                <b>Belum ada data absensi</b>
              </div>
            </div>
          </div>
          @endforelse
          
          <!-- card absen
          <div class="card absen">
            <div class="card-body">
              <div class="card-title">
                <b><i class="fas fa-clock"></i> <span class="mr-3"> 03/17/2021</span></b>
              </div>
              <div class="card-text mt-5">
                <p class="alignleft ml-4">&nbsp Present</p>
                <p class="alignright mr-5">WFO</p>
                <div style="clear: both;"></div>
              </div>
            </div>
          </div>
          card absen -->
          
        </div>
      </div>
    </div>
  </div>    

@endsection