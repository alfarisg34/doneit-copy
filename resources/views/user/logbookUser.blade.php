@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/logbook-user.css') }}">
@endsection

@section('custom_title')
    <title>Daftar Tugas Pegawai</title>
@endsection('custom_title')

@section('content')
<div class="container-fluid">
    <h1 class="judul-section">Logbook</h1>

    <div class="row mt-3">
        <div class="col-sm-4 date-status">
            <h2 class="date-status">Undone Task</h2>
            <h5 class="date-status">Date : {{$date}}</h5>
        </div>
        <div class="col-sm-8">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Task</th>
                    <th scope="col" class="text-center">Forum</th>
                    <th scope="col" class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($unDone as $tugasBelum)
                    <tr>
                    <td><a class="task-link" href="#" data-id="{{ $tugasBelum->id }}" data-toggle="modal" data-target="#">{{$tugasBelum -> title}}</a></td>
                    <td class="text-center"><a class="task-link" href="/detailforum/{{$tugasBelum -> id}}"><i class="fas fa-comment-alt"></i></a></td>
                    <td class="text-center">
                        <form method="POST" action="/logbook/{{$tugasBelum->id}}">
                            @csrf
                            <input class="form-check-input" name="checked" type="checkbox" value="50" onChange="this.form.submit()">
                        </form>
                    </td>
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4">Tidak Ada Data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-12">
            <h2 class="date-status">Done Task</h2>
            <table class="table w-100">
                <thead>
                    <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Task</th>
                    <th scope="col" class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($done as $tugasSelesai)
                    <tr>
                    <td>{{$tugasSelesai->dueTo}}</td>
                    <td><a class="task-link link-done" href="#" data-id="{{ $tugasSelesai->id }}" data-toggle="modal" data-target="#">{{$tugasSelesai->title}}</a></td>
                    @if($tugasSelesai->progress < 100 )
                        @if($tugasSelesai->progress < 50)
                        <td class="text-center notyet">Not Yet</td>
                        @else
                        <td class="text-center onreview">On Review</td>
                        @endif
                    @else
                        <td class="text-center done">Done</td>
                    @endif
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4">Tidak Ada Data</td>
                    </tr>
                    @endforelse
                </tbody>
        </table>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalDone" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

            </div>
        </div>
    </div>

</div>

@section('custom_js_bottom')
    <script>
        $('.task-link').on('click',function(){
            let id = $(this).data('id')
            $.ajax({
                url:`logbook/detail/${id}`,
                mehod:"GET",
                success: function(data){
                    $('#modalDone').find('.modal-content').html(data)
                    $('#modalDone').modal('show')
                },
                error:function(error){
                    console.log(error)
                }
            }) 
        })
    </script>
@endsection('custom_js_bottom')
@endsection('content')