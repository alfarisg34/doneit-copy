@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/forum.css') }}">
@endsection

@section('custom_title')
    <title>Active Forum</title>
@endsection('custom_title')

@section('content')
    <div class="container-fluid">
        <h1 class="judul-section">Active Forum</h1>

        <div class="col-md-12">
        @forelse($forums as $forumUser)
            <div class="box mt-5">
                <h1 style="color:#3A728D;"class="judul-kotak">Discussion</h1>
                <h1>{{$forumUser->title}}</h1>
                <form action="/absen" method="post">
                    <a href="/detailforum/{{$forumUser->id}}" class="btn-forum">
                        Go To Forum
                    </a>
                </form>
            </div>
        @empty
        <div class="box mt-5">
            <h1 class="judul-kotak">Discussion</h1>
            <h1>Tidak ada dikusi</h1>
        </div>
        @endforelse
        </div>
    </div>
@endsection('content')