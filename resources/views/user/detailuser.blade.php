@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/detailforumadmin.css') }}">
@endsection

@section('custom_title')
    <title>DoneIt! | Aplikasi Manajemen Organisasi</title>
@endsection('custom_title')

@section('content')
<div class="container">
    <div class="outer-box" style="margin-top: 120px;">
        <div class="header-box">
            @foreach ($forums as $forums)
            <div class="row">
                <div class="col-2">
                    <h1><i class="far fa-comments"></i></h1>    
                </div>
                <div class="col-6">
                    <h5>Discussion:</h5>
                    <h2>{{$forums->title}}</h2>
                </div>
                <div class="col">
                    <p class="float-right">{{$forums->date}}</p>
                </div>
            </div>
            @endforeach
        </div>
        <hr>

        <div class="row">
            <div class="col-12">
                <div class="card">
                <form action="/detailforum/{{$forums -> id}}" method="post">
                @csrf
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset('/assets/img/profile-foto.jpg') }}" class="rounded-circle" alt="Cinque Terre"  width="80" height="80">
                        </div>
                        <div class="col">
                            <textarea style="height:110px;" class="form" type="text" id="desc" name="desc" placeholder="Hei, What's on your mind?"></textarea>
                        </div>
                    </div>
                    <hr class="mt-5">
                    <div class="float-right">
                        <button type="submit" class="btn btn-success float-right">
                            Post
                        </button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12">
                <h5><strong>Replies</strong></h5>
                @forelse ($reply as $reply)
                <div class="card mt-5">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset('/assets/img/profile-foto.jpg') }}" class="rounded-circle" alt="Cinque Terre"  width="80" height="80">
                        </div>
                        <div class="col mt-3">
                            <p class="profile-name">{{$reply->name}}</p>
                            <p>{{$reply->desc}}</p>
                        </div>
                    </div>
                </div>
                @empty
                <div class="card">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset('/assets/img/profile-foto.jpg') }}" class="rounded-circle" alt="Cinque Terre"  width="80" height="80">
                        </div>
                        <div class="col mt-3">
                            <p>Tidak ada Comment</p>
                        </div>
                    </div>
                </div>
                @endforelse
            </div>
        </div>

    </div>
</div>
@endsection('content')