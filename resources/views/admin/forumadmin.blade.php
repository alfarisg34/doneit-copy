@extends('layout.admin')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/forumadmin.css') }}">
@endsection

@section('custom_title')
    <title>Monitor Forum</title>
@endsection('custom_title')

@section('content')
<div class="container-fluid">
<h1 class="judul-section">Active Forum</h1>
    @forelse($forums as $forums)
      <div class="shape">
        <p class="title">Discussion:</p>
        <div id="container">
          <p><b>{{ $forums -> title }}</b></p>
          <div class="float-right">
            <a href="/detailforumadmin/{{ $forums -> id}}" class="button">Go to Forum</a>
          </div>
        </div>
      </div>
    @empty
      <div class="shape">
          <div id="container">
            <p><b>Tidak Ada Diskusi</b></p>
          </div>
        </div>
    @endforelse
</div>

@endsection('content')