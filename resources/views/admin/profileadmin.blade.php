@extends('layout.admin')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/profile.css') }}">
@endsection

@section('custom_title')
    <title>Profile Admin</title>
@endsection('custom_title')

@section('content')
<div class="container-fluid admin">
    <div class="separator-home"></div>
        
        <!-- User profile -->
        <div class="green-background">
        <div class="col">
        <div class="card-green">
            <div class="row">
            <div class="col">
                <div class="float-left">
                <p class="card-title">Profile</p>
                </div>
                <div class="float-right">
                <a href="#" data-toggle="modal" data-target="#modalProfile" class="float-right"> <i class="fas fa-cog"></i></a>
                </div>
            </div>
            </div>
            
            <div class="separator-md"></div>
            <div class="row">
            <div class="col-xs-12 col-md-2">
            <!-- <div class="col-xs-offset-4 col-xs-4 col-md-2"></div> -->
            <img src="{{ asset('/assets/img/profile-foto.jpg') }}" class="img-rounded" width="100%">
            <img src="{{ asset('/assets/img/camera.png') }}" class="-right" id="edit-photo-icon">
            </div> 
            <div class="col-xs-12 col-md-8 col-md-offset-1">
            <div class="separator-mob"></div>
            <p class="text-desc">Name</p>
            @if(($user->name)!= NULL)
                <p class="text-isi">{{ old('name', $user->name) }}</p>
            @else
                <p class="text-isi">-</p>
            @endif
            
            <p class="text-desc">Address</p>
            @if(($user->address)!= NULL)
                <p class="text-isi">{{ old('address', $user->address) }}</p>
            @else
                <p class="text-isi">-</p>
            @endif

            <p class="text-desc">Email</p>
            @if(($user->email)!= NULL)
                <p class="text-isi">{{ old('email', $user->email) }}</p>
            @else
                <p class="text-isi">-</p>
            @endif

            <p class="text-desc">Phone Number</p>
            @if(($user->phone)!= NULL)
                <p class="text-isi">{{ old('phone', $user->phone) }}</p>
            @else
                <p class="text-isi">-</p>
            @endif

            <p class="text-desc">Company</p>
            @if(($user->company)!= NULL)
                <p class="text-isi">{{ old('company', $user->company) }}</p>
            @else
                <p class="text-isi">-</p>
            @endif
            <!-- <div class="line"></div> -->
            <!-- <form>
            <p class="text-pw">Current Password</p>
            <input class="form-pw" type="password" id="password" name="password"></br>
                <div class="separator-md"></div>
            <p class="text-pw">New Password</p>
            <input class="form-pw" type="password" id="password" name="password"></br>
            <div class="separator-md"></div>
            
            <button type="submit" class="button-submit">Change Password</button>
            <div class="separator-mob"></div>
                
            </form> -->
            </div>
        </div>
        </div>
        </div>
        <!-- End user profile -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            
            <form method="POST" action="{{ route('user.profile') }}" class="form mt-3">
                @method('patch')
                @csrf
                <h5 class="modal-title mb-3">Update Profile</h5>
                
                <label for="name" class="text-pw">{{ __('Name') }}</label></br>
                <input id="name" type="text" class="form-pw @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <div class="separator-md"></div>

                <label for="address" class="text-pw">{{ __('Address') }}</label><br>
                <input id="address" type="text" class="form-pw @error('address') is-invalid @enderror" name="address" value="{{ old('address', $user->address) }}" autocomplete="address" autofocus>
                @error('address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="separator-md"></div>

                <label for="email" class="text-pw">{{ __('E-Mail Address') }}</label><br>
                <input id="email" type="email" class="form-pw @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" autocomplete="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="separator-md"></div>

                <label for="phone" class="text-pw">{{ __('Phone Number') }}</label><br>
                <input id="phone" type="text" class="form-pw @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone', $user->phone) }}" autocomplete="phone">
                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="separator-md"></div>

                <label for="company" class="text-pw">{{ __('Company') }}</label><br>
                <input id="company" type="text" class="form-pw @error('company') is-invalid @enderror" name="company" value="{{ old('company', $user->company) }}" autocomplete="company">
                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="separator-md"></div>

                <button type="submit" class="button-submit">Perbarui</button>
                <div class="separator-mob"></div>
                
            </form>
            </div>
        </div>
        </div>
    </div>

</div>
@endsection('content')