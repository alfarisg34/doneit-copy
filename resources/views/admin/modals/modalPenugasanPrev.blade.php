<div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <br>
    <h3 class="text-center judul-modal">{{$dataTugas->title}}</h3>
    <hr class="col-sm-8">
    <div class="modal-description">
        <p>
        {{$dataTugas->desc}}
        </p>
    </div>
    <div class="modal-status">
        <table class="table-bordered w-100">
        <tbody>
            <tr>
                <td class="desc-title w-25">Due Date</td>
                <td>{{$dataTugas->dueTo}}</td>
            </tr>
            <tr>
                <td class="desc-title w-25">Status</td>
                @if($dataTugas->progress < 100 )
                    @if($dataTugas->progress < 50)
                    <td class="notyet">Not Yet</td>
                    @else
                    <td class="onreview">Need Approval</td>
                    @endif
                @else
                    <td class="done">Done</td>
                @endif
            </tr>
        </tbody>
        </table>
    </div>
</div>