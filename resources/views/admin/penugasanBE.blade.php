<a href="/dashboard">back</a>
<form method="POST" action="/penugasan" class="pull-right"> 
    @csrf
    <input class="form" type="text" id="title" name="title" placeholder="Title"></br>
  <div class="separator"></div>
  <input class="form" type="text" id="desc" name="desc" placeholder="Description"></br>
  <div class="separator"></div>
  <input class="form" type="date" id="dueTo" name="dueTo" placeholder="Due To"></br>
  <div class="separator"></div>
  <input class="form" type="text" id="nip" name="user_id" placeholder="NIP"></br>
  <div class="separator"></div>
  <div class="separator-md"></div>
  <button type="submit" class="btn btn-primary button-form">Submit</button>
</form>

<div class="panel-body">
  <table class="table table-responsive table-bordered table-hover">
      <thead>
          <tr>
              <th>Date</th>
              <th>Task</th>
              <th>Progress</th>
          </tr>
      </thead>
      <tbody>
         @forelse($tugas as $tugas)
              <tr>
                  <td>{{$tugas->dueTo}}</td>
                  <td>{{$tugas->title}}</td>
                  <td>{{$tugas->progress}}</td>
              </tr>
          @empty
              <tr>
                  <td colspan="4"><b><i>TIDAK ADA DATA UNTUK DITAMPILKAN</i></b></td>
              </tr>
          @endforelse 
      </tbody>
  </table>
</div>