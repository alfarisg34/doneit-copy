@extends('layout.admin')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/dashboard.css') }}">
@endsection

@section('custom_js_head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.min.js" integrity="sha512-VMsZqo0ar06BMtg0tPsdgRADvl0kDHpTbugCBBrL55KmucH6hP9zWdLIWY//OTfMnzz6xWQRxQqsUFefwHuHyg==" crossorigin="anonymous"></script>
@endsection('custom_js_head')

@section('custom_title')
    <title>Dashboard</title>
@endsection('custom_title')

@section('content')
<div class="container-flex">
    <div class="row">
        <div class="col-6">
            <div class="graph-productivity">
                <h3>Employee Productivity</h3>
                <a href="#" class="button">2021</a>
                <canvas id="myChart" style="width:auto;"></canvas>
            </div>
        </div>
        <div class="col-3">
            <div class="container-achievement">
                <img src="{{ asset('/assets/img/achievement.svg') }}" alt="achievement-thumbnail">
                <div class="achievement">
                    <h3>Achievements</h3>
                    <p>See your employees <br> performance</p>
                    <a href="#" class="button">
                        More <i class="fas fa-chevron-right ml-3"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-3">
            @include('admin.dashboard.discussion')
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-4">
            @include('admin.dashboard.logActivity')
        </div>
        <div class="col-8">
            @include('admin.dashboard.presence')
        </div>
    </div>
</div>
@endsection('content')

@section('custom_js_bottom')
    <script>
        var xValues = <?php echo $tanggalJson; ?>;
        var yValues = <?php echo $avg; ?>;

        console.log(xValues);

        new Chart("myChart", {
        type: "line",
        data: {
            labels: xValues,
            datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(124, 182, 50, 1)",
            borderColor: "rgba(58, 114, 141, 0.83)",
            data: yValues
            }]
        },
        options: {
            legend: { display: false },
            scales: {
                yAxes: [{ ticks: { min: 6, max: 16 } }],
            }
        }
        });
    </script>
@endsection('custom_js_bottom')