<div class="presence">
    <h3 class="float-left">Employee Presence</h3>
    <p class="float-right">{{$date}}</p>
    <table class="table table-striped">
        <thead>
            <tr>
            <th scope="col">Name</th>
            <th scope="col" class="text-center">Check-In</th>
            <th scope="col" class="text-center">Check-Out</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data_absen as $key)
            <tr>
            <td class="name"><img src="{{ asset('/assets/img/profile-foto.jpg') }}" alt="profile-photo" class="img-rounded mr-1"><b>{{$key->name}}</b></td>
            <td class="text-center">{{$key->time_in}}</td>
            <td class="text-center">{{$key->time_out}}</td>
            </tr>
            @empty
            <tr>
            <td colspan="4">Belum ada data absensi</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>