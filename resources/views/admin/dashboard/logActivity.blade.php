<div class="log-activity">
    <h3>Employee Log Activity</h3>
    @forelse($logActivity as $key)
    <div class="row">
        <div class="col-2">
            <img src="{{ asset('/assets/img/profile-foto.jpg') }}" alt="profile-photo" class="img-rounded">
        </div>
        <div class="col-8">
            <p>{{$key->name}}</p>
            <p>{{$key->title}}</p>
        </div>
        <div class="col-2">
        @if($key->progress < 100 )
            @if($key->progress < 50)
            <i class="fas fa-times notyet"></i>
            @else
            <i class="fas fa-file-signature onreview"></i>
            @endif
        @else
        <i class="fas fa-check done"></i>
        @endif
        </div>
    </div>
    <hr>
    @empty
    <div class="row">
        <div class="col">
            <p>Belum Ada Data</p>
        </div>
    </div>
    <hr>
    @endforelse
</div>