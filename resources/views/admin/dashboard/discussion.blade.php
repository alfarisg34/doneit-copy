<div class="discussion">
    <h3>Discussion</h3>
    @php($i=0)
    @forelse($data_forum as $key)
    <div class="row">
        <div class="col-2">
            <img src="{{ asset('/assets/img/profile-foto.jpg') }}" alt="profile-photo" class="img-rounded">
        </div>
        <div class="col-8">
            <p><strong><a href="/detailforumadmin/{{$key->id}}">{{$key->title}}</a></strong></p>
        </div>
        <div class="col-2">
            @if($replycount[$i] > 0)
            <div class="text-center number">
                {{$replycount[$i]}}
            </div>
            @else
            @endif
        </div>
    </div>
    <hr>
    @php($i++)
    @empty
    <div class="row">
        <div class="col">
            <p>Belum Ada Diskusi</p>
        </div>
    </div>
    <hr>
    @endforelse
</div>