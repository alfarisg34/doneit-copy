@extends('layout.admin')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/dashboard.css') }}">
@endsection

@section('custom_title')
    <title>Dashboard</title>
@endsection('custom_title')

@section('content')
  
  
  <h1>ini dashboard</h1>
  <h5>{{ Auth::user()->name }}</h5>
  <h5>{{ Auth::user()->user_id }}</h5>
  <a href="/logout">Logout</a>

      <a href="/penugasan">Penugasan</a>


  <div class="panel-body">

    {{-- {{$absen->date}} --}}

    <table class="table table-responsive table-bordered table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Tanggal</th>
                <th>Check-in</th>
                <th>Check-out</th>
            </tr>
        </thead>
        <tbody>
           @forelse($data_absen as $absen)
                <tr>

                    <td>{{$absen->user->name}}</td>
                    <td>{{$absen->date}}</td>
                    <td>{{$absen->time_in}}</td>
                    <td>{{$absen->time_out}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4"><b><i>TIDAK ADA DATA UNTUK DITAMPILKAN</i></b></td>
                </tr>
            @endforelse 
        </tbody>
    </table>
   {{-- {!! $data_absen->links() !!}  --}}
</div>

  @endsection