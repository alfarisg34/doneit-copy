@extends('layout.admin')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/rekapabsen.css') }}">
@endsection

@section('custom_title')
    <title>Absen Pegawai</title>
@endsection('custom_title')

@section('content')
<div class="container-fluid">
    <h1 class="judul-section">Employee Presence's Recap</h1>

    <!-- Card Rekap -->
    @php($i=0)
    @forelse($hasil_absen as $absen)
        <div class="card-box mb-5">
            <div class="row">
                <div class="col card-desc">
                    <h3 class="mb-5">
                        <strong>
                            Date <span class="mr-5">{{$tanggal[$i]->date}}</span>
                        </strong>
                    </h3>
                    <h3>Present <span class="mr-5">:{{$present[$i]}} People</span></h3>
                    <h3>Absent <span class="mr-5">: {{$absent[$i]}} People</span></h3>
                </div>
                <div class="col">
                    <div class="table-out">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col" class="text-center">Check-In</th>
                                <th scope="col" class="text-center">Check-Out</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($absen as $hariini)
                                <tr>
                                <td class="name"><img src="{{ asset('/assets/img/profile-foto.jpg') }}" alt="profile-photo" class="img-rounded mr-1"> {{$hariini->name}}</td>
                                <td class="text-center">{{$hariini->time_in}}</td>
                                <td class="text-center">{{$hariini->time_out}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @php($i++)
    @empty
    <div class="card-box mb-5">
            <div class="row">
                <div class="col">
                    <h3><strong>Belum Ada Data absensi</strong></h3>
                </div>
            </div>
    </div>
    @endforelse

</div>
@endsection('content')