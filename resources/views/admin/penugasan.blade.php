@extends('layout.admin')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/penugasan.css') }}">
@endsection

@section('custom_title')
    <title>Tugas Pegawai</title>
@endsection('custom_title')

@section('content')
<div class="container-fluid">
    <h1 class="judul-section">Employee's Logbook</h1>

    <div class="row mt-3">
        <div class="col-sm-4 date-status">
            <h2 class="date-status">Today's Task</h2>
            <h5 class="date-status">Date : {{$date}}</h5>
            <button type="submit" class="btn-add task-link" href="#" data-toggle="modal" data-target="#modalAddTask">Add Task</button>
        </div>
        <div class="col-sm-8">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Task</th>
                    <th scope="col" class="text-center">Forum</th>
                    <th scope="col" class="text-center">Progress</th>
                    <th scope="col" class="text-center">Approve</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($tugas as $tugasSekarang)
                    @if($tugasSekarang -> dueTo >= $date)
                    <tr>
                        <td><a class="task-link-tugasnow" href="#" data-id="{{$tugasSekarang->id}}" data-toggle="modal" data-target="#">{{$tugasSekarang->title}}</a></td>
                        <td class="text-center"><a href="/detailforumadmin/{{$tugasSekarang -> id}}"><i class="fas fa-comment-alt"></i></a></td>
                        @if($tugasSekarang->progress < 100 )
                            @if($tugasSekarang->progress < 50)
                            <td class="text-center notyet">Not Yet</td>
                            @else
                            <td class="text-center onreview">Need Approval</td>
                            @endif
                        @else
                            <td class="text-center done">Done</td>
                        @endif
                        <td class="text-center">
                            <form method="POST" action="/penugasan/{{$tugasSekarang->id}}">
                                @csrf
                                @if($tugasSekarang->progress < 50)
                                @else
                                <input class="form-check-input" name="checked" type="checkbox" value="100" onChange="this.form.submit()" {{$tugasSekarang->progress > 50 ? 'checked disabled' : ''}} >
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endif
                    @empty
                    <tr>
                        <td colspan="4"><b><i>TIDAK ADA DATA UNTUK DITAMPILKAN</i></b></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-sm-12">
            <h2 class="date-status">Previous Task</h2>
            <table class="table w-100">
                <thead>
                    <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Task</th>
                    <th scope="col" class="text-center">Progress</th>
                    <th scope="col" class="text-center">Approve</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($tugas as $tugas)
                    @if($tugas -> dueTo < $date)
                    <tr>
                        <td>{{$tugas -> dueTo}}</td>
                        <td><a class="task-link-tugas" href="#" data-id="{{$tugas->id}}" data-toggle="modal" data-target="#">{{$tugas -> title}}</a></td>
                        @if($tugas->progress < 100 )
                            @if($tugas->progress < 50)
                            <td class="text-center notyet">Not Yet</td>
                            @else
                            <td class="text-center onreview">Need Approval</td>
                            @endif
                        @else
                            <td class="text-center done">Done</td>
                        @endif
                        <td class="text-center">
                            <form method="POST" action="/penugasan/{{$tugas->id}}">
                                @csrf
                                @if($tugas->progress < 50)
                                @else
                                <input class="form-check-input" name="checked" type="checkbox" value="100" onChange="this.form.submit()" {{$tugas->progress>50 ? 'checked disabled' : ''}} >
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endif
                    @empty
                    <tr>
                        <td colspan="4"><b><i>TIDAK ADA DATA UNTUK DITAMPILKAN</i></b></td>
                    </tr>
                    @endforelse
                </tbody>
        </table>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalNow" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                
            </div>
        </div>
    </div>

    <!-- Modal Prev -->
    <div class="modal fade bd-example-modal-lg" id="modalPrevTask" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                
            </div>
        </div>
    </div>
    
    <!-- Modal Add Task-->
    <div class="modal fade bd-example-modal-lg" id="modalAddTask" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <br>
                    <h3 class="text-center judul-modal">Add Task</h3>
                    <hr class="col-sm-10">
                    <div class="modal-status">
                        <table class="w-100">
                        <form action="/penugasan" method="post">
                        @csrf
                        <tbody class="none-back">
                            <tr>
                                <td class="desc-title w-28 float-right">Pegawai : </td>
                                <td>
                                <select name="user_id" id="user_id" class="form">
                                    @forelse($users -> where('role_id','0') as $user)
                                        <option value="{{$user -> user_id}}">{{$user -> name}}</option>
                                    @empty
                                        <option value="0">Tidak Aada Pegawai
                                    @endforelse
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="desc-title w-28 float-right">Title : </td>
                                <td>
                                <input class="form" type="text" id="title" name="title"></input>
                                </td>
                            </tr>
                            <tr>
                                <td class="desc-title w-28 float-right">Description : </td>
                                <td><textarea style="height:110px;" class="form" type="text" id="desc" name="desc"></textarea></td>
                            </tr>
                            <tr>
                                <td class="desc-title w-28 float-right">Due Date : </td>
                                <td><input class="form" type="date" id="dueTo" name="dueTo"></input></td>
                            </tr>
                            <tr>
                                <td class="desc-title w-28 float-right">Status : </td>
                                <td><input type="radio" id="status" name="status" value="WFO"> WFO</input></br>
                                <input type="radio" id="status" name="status" value="WFH"> WFH</input></td>
                            </tr>
                            <tr>
                                <td class="float-right"><button type="submit" class="btn-add task-link">Add Task</button></td>
                            </tr>
                        </tbody>
                        </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


</div>

@section('custom_js_bottom')
    <script>
        $('.task-link-tugasnow').on('click',function(){
            let id = $(this).data('id')
            $.ajax({
                url:`penugasan/now/${id}`,
                mehod:"GET",
                success: function(data){
                    $('#modalNow').find('.modal-content').html(data)
                    $('#modalNow').modal('show')
                },
                error:function(error){
                    console.log(error)
                }
            }) 
        })
        $('.task-link-tugas').on('click',function(){
            let id = $(this).data('id')
            $.ajax({
                url:`penugasan/previous/${id}`,
                mehod:"GET",
                success: function(data){
                    $('#modalPrevTask').find('.modal-content').html(data)
                    $('#modalPrevTask').modal('show')
                },
                error:function(error){
                    console.log(error)
                }
            }) 
        })
    </script>
@endsection('custom_js_bottom')

@endsection('content')