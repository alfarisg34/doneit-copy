<nav class="navbar fixed-top navbar-light navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="navbar-brand" href="/">
            <img src="{{ asset('/assets/img/logo-doneit-bener.png') }}" width="150" height="50" alt="logo-thumbnail">
          </a>
        </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
    @if (Auth::user() != null)
      @if (Auth::user()->isAdmin())
      <li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }}"> 
        <a class="nav-link" href="/absen">Dashboard</a>
      </li>
      <li class="nav-item {{ Request::is('profile') ? 'active' : '' }}">
        <a class="nav-link" href="/profile">Profile</a>
      </li>
      @else
      <!-- navbar user -->
      <li class="nav-item {{ Request::is('absen') ? 'active' : '' }}"> 
        <a class="nav-link" href="/absen">Absent</a>
      </li>
      <li class="nav-item {{ Request::is('profile') ? 'active' : '' }}">
        <a class="nav-link" href="/profile">Profile</a>
      </li>
      <li class="nav-item {{ Request::is('forum') ? 'active' : '' }}">
        <a class="nav-link" href="/forum">Forum</a>
      </li>
      <li class="nav-item {{ Request::is('logbook') ? 'active' : '' }}">
        <a class="nav-link" href="/logbook">Logbook</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Hi,  {{ Auth::user()->name }}! &nbsp <img src="{{ asset('/assets/img/profile-foto.jpg') }}" alt="foto-user" width="30" height="30" class="rounded-circle">
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt mr-1"></i> Logout</a>
        </div>
      </li>
      <!-- navbar user -->
      @endif
    @else
      <!-- navbar before login -->
      <li class="nav-item">
        <a class="nav-link" href="/login">Log in</a>
      </li>
      <li class="nav-item">
        <a class="nav-link btn btn-outline-dark" href="/register">Sign Up</a>
      </li>    
      <!-- navbar before login -->
    @endif
    </ul>
    </div>
  </nav>