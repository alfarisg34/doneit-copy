<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/Bootstrap/bootstrap.min.css') }}">

        <!-- Main Font -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@700&display=swap" rel="stylesheet">

        <!-- FontAwesome 5 -->
        <link href="{{ asset('assets/FontAwesome/css/all.css') }}" rel="stylesheet">

        <!-- main css -->
        <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">

        <!-- ajax -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- custom css -->
        @yield('custom_css')

        <!-- custom script -->
        @yield('custom_js_head')

        <!-- title -->
        @yield('custom_title')
    
    </head>

    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="position-fixed">
                    <div class="sidebar-header">
                        <a href="#">
                            <img src="{{ asset('/assets/img/logo-doneit-puzzle.png') }}" width="55" height="50" alt="logo">
                        </a>    
                    </div>

                    <ul class="list-unstyled components">
                        <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                            <a href="/dashboard">
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                        <li class="{{ Request::is('rekapabsen') ? 'active' : '' }}">
                            <a href="/rekapabsen">
                                <i class="fas fa-history"></i>
                            </a>
                        </li>
                        <li class="{{ Request::is('penugasan') ? 'active' : '' }}">
                            <a href="/penugasan">
                                <i class="fas fa-tasks"></i>
                            </a>
                        </li>
                        <li class="{{ Request::is('forumadmin') ? 'active' : '' }}">
                            <a href="/forumadmin">
                                <i class="fas fa-comment-alt"></i>
                            </a>
                        </li>
                        
                        <div class="bottom-list">
                            <li>
                                <a href="/logout">
                                    <i class="fas fa-sign-out-alt"></i>
                                </a>
                            </li>
                            <li class="{{ Request::is('profileadmin') ? 'active' : '' }}">
                                <a href="/profileadmin">
                                    <i class="fas fa-user"></i>
                                </a>
                            </li>
                        </div>
                    </ul>
                </div>
            </nav>

            <!-- Page Content  -->
            <div id="content">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @yield('content')
                
            </div>
        </div>
        
        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/js/admin.js') }}"></script>
        @yield('custom_js_bottom')

        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        -->
    </body>

</html>



   

