@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/loginregister.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/slick.css') }}">
@endsection

@section('custom_title')
    <title>DoneIt! | Aplikasi Manajemen Organisasi</title>
@endsection('custom_title')

@section('content')

<div class="container mv-large">
<div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="separator-lg"></div>
              <div class="separator-lg"></div>
                <div class="super-big-title">Sign In to</br>
                    Your Account</div>
                    <div class="separator-md"></div>
                    <div class="separator-md"></div>
                      <p class="subsubtitle"> Sign Up Here</p>
          </div>

          
          <div class="col-xs-12 col-sm-6">
            <div class="separator-lg"></div>
              <form method="POST" action="/register" class="float-right"> 
                  @csrf
                  <input class="form" type="text" id="username" name="username" placeholder="Username"></br>
                  <div class="separator"></div>
                  <input class="form" type="email" id="email" name="email" placeholder="Email"></br>
                  <div class="separator"></div>
                  <input class="form" type="text" id="name" name="name" placeholder="Name"></br>
                  <div class="separator"></div>
                  <input class="form" type="text" id="nip" name="user_id" placeholder="NIP"></br>
                  <div class="separator"></div>
                  <input class="form" type="password" id="password" name="password" placeholder="Password"></br>
                  <div class="separator"></div>
                  <input class="form" type="password" id="passwordconf" name="passwordconf" placeholder="Confirm Password"></br>
                  <div class="separator"></div>
                  <div class="separator-md"></div>
                  <button type="submit" class="btn btn-primary button-form">Sign Up</button>
                  <div class="separator"></div>
              </form>
            </div>
          </div>
</div>
    
@endsection