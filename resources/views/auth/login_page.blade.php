@extends('layout.app')
@section('custom_css')
    <link rel="stylesheet" href="{{ asset('/assets/css/loginregister.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/slick.css') }}">
@endsection

@section('custom_title')
    <title>DoneIt! | Aplikasi Manajemen Organisasi</title>
@endsection('custom_title')

@section('content') 

<div class="container mv-large">
  <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="separator-lg"></div>
              <div class="separator-lg"></div>
                <div class="super-big-title">Sign In to</br>
                    Your Account</div>
                    <div class="separator-md"></div>
                    <div class="separator-md"></div>
                    <p>
                      If you don’t have an account</br>
                      You can <a style="color:#1E5372"href="/register">Register here!</a>
                    </p>
              </div>
          
          <div class="col-xs-12 col-sm-6">
            <div class="separator-lg"></div>
              <div class="separator-lg"></div>
              <form method="POST" action="/login" class="float-right"> 
                  @csrf
                  <input class="form @error('email') is-invalid @enderror" type="email" id="email" name="email" placeholder="Email or Username"></br>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                <div class="separator"></div>
                  <input class="form @error('password') is-invalid @enderror" type="password" id="password" name="password" placeholder="Password"></br>
                  @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                  <div class="separator"></div>
                  <div class="separator-md"></div>
                  <button type="submit" class="btn btn-primary button-form">Sign In</button>
                  <div class="separator"></div>
              </form>
              
              </div>
          </div>
  </div>
</div>
