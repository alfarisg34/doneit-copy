<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API;

use App\Http\Controllers\ForumController;
use App\Http\Controllers\AbsenController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get("GetDataForums",[API::class,'getDataForums']);

Route::get("getDataAbsens",[API::class,'getDataAbsens']);

Route::get("getDataUsers",[API::class,'getDataUsers']);

Route::get("getDataReplies",[API::class,'getDataReplies']);