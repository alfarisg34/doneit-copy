<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\AbsenController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LogbookController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\TugasController;
use App\Http\Controllers\ReplyController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//auth
Route::get('/',[AuthController::class,'index']);
Route::get('/login',[AuthController::class,'login'])->name('login');
Route::get('/register',[AuthController::class,'register']);
Route::get('/logout', [AuthController::class,'logout']);

Route::post('/login',[AuthController::class,'loginAction']);
Route::post('/register',[AuthController::class,'registerAction']);

//admin (dashboard)
Route::get('/dashboard', [DashboardController::class,'index']);
Route::get('/penugasan', [TugasController::class,'index']);
Route::get('/penugasan/previous/{id}', [TugasController::class,'modalTaskPrev']);
Route::get('/penugasan/now/{id}', [TugasController::class,'modalTaskNow']);

Route::get('/forumadmin', [ForumController::class,'forumAdmin']);
Route::get('/detailforumadmin/{id}', [ForumController::class,'detailForumAdmin']);
Route::get('/rekapabsen', [AbsenController::class,'absenAdmin']);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/profileadmin', [ProfileController::class,'edit'])->name('admin.profileadmin');
    Route::patch('profile', [ProfileController::class,'update'])
        ->name('profile.update');
});
Route::get('chartjs', [DashboardController::class, 'chart'])->name('chartjs.chart');

Route::post('/penugasan/{id}', [TugasController::class,'adminCheckAction']);
Route::post('/penugasan', [TugasController::class,'tugasAction']);
// Route::post('/penugasan', [LookbookController::class,'adminCheckAction']);
Route::post('/detailforumadmin/{id}', [ForumController::class,'replyActionAdmin']);

//user (homepage)
Route::get('/absen', [AbsenController::class,'absen']);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', [ProfileController::class,'edit'])->name('user.profile');
    Route::patch('profile', [ProfileController::class,'update'])
        ->name('profile.update');
});
Route::post('/absen', [AbsenController::class,'absenAction']);
Route::get('/logbook', [LogbookController::class,'logbook']);
Route::get('/logbook/detail/{id}', [LogbookController::class,'modalTask']);

Route::get('/forum', [ForumController::class,'forum']);
Route::get('/detailforum/{id}', [ForumController::class,'detailforum']);

Route::post('/logbook/{id}', [LogbookController::class,'userCheckAction']);
Route::post('/detailforum/{id}', [ForumController::class,'replyAction']);
