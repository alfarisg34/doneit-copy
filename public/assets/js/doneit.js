// function seeAchievement() {
//   window.scroll({
//     top: 640,
//     behavior: 'smooth'
//   });
// }
// $(document).ready(function(){
//   $('#slider-slick').slick({
//     infinite: true,
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     responsive: [{
//       breakpoint: 629,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }]
//   });
// });
// $(document).ready(function(){
//   $('#slider-slick').slick({
//     infinite: false,
//     speed: 300,
//     slidesToShow: 3,
//     slidesToScroll: 3,
//     nextArrow: '<i class="fas fa-arrow-circle-right slick-next"></i>',
//     prevArrow: '<i class="fas fa-arrow-circle-left slick-prev"></i>',
//     responsive: [
//       {
//         breakpoint: 1024,
//         settings: {
//           slidesToShow: 3,
//           slidesToScroll: 3,
//           infinite: true,
//         }
//       },
//       {
//         breakpoint: 600,
//         settings: {
//           slidesToShow: 2,
//           slidesToScroll: 2,
//           infinite: true
//         }
//       },
//       {
//         breakpoint: 480,
//         settings: {
//           slidesToShow: 1,
//           slidesToScroll: 1,
//           infinite: true
//         }
//       }
//     ]
//   });
// });

$(document).ready(function(){
  $('#slider-slick').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
});


// $(document).ready(function(){
//   $('#support').slick({
//     infinite: true,
//     slidesToShow: 2,
//     slidesToScroll: 1,
//     responsive: [{
//       breakpoint: 480,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }]
//   });
// });

// $(document).ready(function(){
//   $('#support1').slick({
//     infinite: true,
//     slidesToShow: 4,
//     slidesToScroll: 1,
//     responsive: [{
//       breakpoint: 629,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }]
//   });
// });

// $(document).ready(function(){
//   $('#support2').slick({
//     infinite: true,
//     slidesToShow: 5,
//     slidesToScroll: 1,
//     responsive: [{
//       breakpoint: 629,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }]
//   });
// });

// function TestsFunction() {
//   var T = document.getElementById("TestsDiv"),
//       displayValue = "";
//   if (T.style.display == "")
//       displayValue = "none";

//   T.style.display = displayValue;
// }

// function TestsFunction1() {
//   var T = document.getElementById("TestsDiv1"),
//       displayValue = "";
//   if (T.style.display == "")
//       displayValue = "none";

//   T.style.display = displayValue;
// }