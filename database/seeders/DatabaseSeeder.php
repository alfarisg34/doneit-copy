<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user_id' => '12340',
            'username' => 'admin',
            'name' => 'admin',
            'email' => 'admin'.'@gmail.com',
            'password' => Hash::make('admin'),
            'role_id' => '1',
        ]);
        DB::table('users')->insert([
            'user_id' => '12342',
            'username' => 'user1',
            'name' => 'user1',
            'email' => 'user1'.'@gmail.com',
            'password' => Hash::make('user1'),
            'role_id' => '0',
        ]);
        DB::table('users')->insert([
            'user_id' => '12341',
            'username' => 'user2',
            'name' => 'user2',
            'email' => 'user2'.'@gmail.com',
            'password' => Hash::make('user2'),
            'role_id' => '0',
        ]);
    }
}